import h5py


# h5py.run_tests()
# http://docs.h5py.org/en/stable/high/dataset.html#filter-pipeline

class Fast5Filter():
    """This class filters the right reads from the multi fast5 files and writes them to output.fast5.
    It contains the following methods: file_reader, get_DWV_reads and write_to_fast5_file. """

    def file_reader(self):
        """
        This method reads the multi fast5 file and filters the correct reads. These reads (Group) are then written to
        an output file called output.fast5 with the write_to_fast5_file method.
        :return:
        """
        filename = '/homes/jfhaaijer/thema11/beevirus/Sprint1/data/samples/FAL66274_61214d90674fb476eeea0df13759443ace338524_8.fast5'
        id_list = self.get_DWV_ids()

        with h5py.File(filename, 'r') as f:
            # # List all groups
            # print("Keys: %s" % f.keys())
            # https://stackoverflow.com/questions/28170623/how-to-read-hdf5-files-in-python
            for key in f.keys():
                print(key)
            #
            #     # print(f[key]["Raw"].value)
                if key[5:] in id_list:
                    self.write_to_fast5_file(key, f[key])
                for i in f[key]:
                    print(i)
            #     for i in f.items():
            #         print(i)
            #         for a in i:
            #             print(a)
            #         break

            print(f.attrs.itervalues())

            # is_dataset = isinstance(f, h5py.Dataset)
            a_group_key = list(f.keys())[0]
            f.close()


        return a_group_key


    def get_DWV_ids(self):
        """
        This method creates list with the DWV ids from a file.
        Input: File with read ids
        Output: List with Ids
        :return: id_list (List with id's)
         """
        id_file = '/homes/jfhaaijer/thema11/beevirus/Sprint1/data/DWV_read_ids.txt'
        with open(id_file) as f:
            id_list = f.read().splitlines()
        return id_list

    def filter_right_data(self):
        """
        This method
        :return:
        """
        return


    def write_to_fast5_file(self, key, value):
        """
        This method writes the read (Group) to the output.fast5 file. Output.fast5 is a multi fast5 that contains the
        correct reads.
        Input: read Group?
        :param key:
        :param value:
        :return: output.fast5 (multi fast5)
        """
        with open("/homes/jfhaaijer/thema11/beevirus/Sprint1/data/output.fast5", "a") as myfile:
            myfile.write(key)
            myfile.write("\n")
            for i in value:
                myfile.write(i)
                myfile.write("\n")
        return


if __name__ == '__main__':
    f = Fast5Filter()
    f.file_reader()


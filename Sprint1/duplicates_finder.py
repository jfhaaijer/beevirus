import h5py
import numpy as np
from os import listdir
from os.path import isfile, join

# h5py.run_tests()
# http://docs.h5py.org/en/stable/high/dataset.html#filter-pipeline

class Fast5Filter():

    def file_reader(self):
        """
        maakt een lijst van alle file namen
        gebruikt elke file om de read_ids van alle files in een lijst te zetten.
        """
        #mypath = '/commons/Themas/Thema11/Minion2020/fast5_pass/'
        #onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        #print(onlyfiles)
        #print(len(onlyfiles))
        filename = '/students/2019-2020/Thema11/minion_js_jh/data/filtered_reads/batch0.fast5'
        id_list = self.get_DWV_ids()
        a_group_key =  []
        count_a_group_key = 0

        #for file in onlyfiles:
            #filename = '{}{}'.format(mypath,file)
        #for filename in onlyfiles:
        with h5py.File(filename, 'r') as f:
            # # List all groups
            # https://stackoverflow.com/questions/28170623/how-to-read-hdf5-files-in-python

            a_group = [key[5:] for key in f.keys()]
            count_a_group_key += (len(a_group))
            a_group_key += a_group


            print(count_a_group_key)
            f.close()
        return a_group_key

    def get_DWV_ids(self):
        """
        Creates list with the DWV ids from a file.
        Input: File with read ids
        Output: List with Ids
        """
        id_file = '/homes/jdsilvius/jaar_3/thema11/praktijkopdr_11/beevirus/Sprint1/data/DWV_read_ids.txt'
        with open(id_file) as f:
            ids_list = f.read().splitlines()

        print(len(ids_list))
        id_list = self.unique(ids_list)
        return id_list


    def filter_right_data(self, a_group_key, id_list):
        """
        haalt alle unique uit de lijst met alle read_ids
        als een read_id uit de lijst met alle read_ids dan wordt die read_id toegevoegd aan de lijst met gefilterde ids.
        """
        right_ids =[]
        count = 0
        unique_a_group_key = self.unique(a_group_key)
        for i in unique_a_group_key:
            if i in id_list:
                count+=1
                print(count)
                right_ids.append(i)

        print("lengte file ids =", len(a_group_key))
        print("lengte unique file ids =", len(unique_a_group_key))
        print("lengte unique dwv ids =", len(id_list))
        print("lengte filtered ids =", len(right_ids))
        unique_id = self.unique(right_ids)
        print("lengte unique right_ids =", len(unique_id))
        return right_ids

    # function to get unique values
    def unique(self, right_ids):
        x = np.array(right_ids)
        return np.unique(x)


if __name__ == '__main__':
    f = Fast5Filter()
    a_group_key = f.file_reader()
    id_list = f.get_DWV_ids()
    f.filter_right_data(a_group_key, id_list)
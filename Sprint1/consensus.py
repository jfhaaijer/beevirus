<<<<<<< HEAD
#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import operator
import random
"""
from Bio.Seq import Seq
from Bio.Align.Generic import Alignment
from Bio.Align import AlignInfo
from Bio.Alphabet import IUPAC, Gapped
  
from Bio._py3k import raise_from 
from Bio.Seq import Seq 
from Bio.SeqRecord import SeqRecord, _RestrictedDict 
from Bio import Alphabet 
from Bio.Align import _aligners 

align = AlignIO.read("/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/region_output.fa", "fasta") 
summary_align = AlignInfo.SummaryInfo(align)
consensus = summary_align.dumb_consensus()
"""

sequence = ''
op_file = open("/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/region_output.fa")
for line in op_file:
	if line.startswith(">"):
		line = line.replace(line, "#")
		sequence += line
	else:
		sequence += line
sequence = sequence.replace("\n", "")
sequences = sequence.split("#")

direc = {}

for seq in sequences:
	g = 0
	for a in seq:
		g += 1
		# dictionary met elke posite de actgoverige als keys en aantal als value.
		pos = g-1

		nuc_key = {
			"M": random.choice("AC"), "R": random.choice("AG"), "W": random.choice("AT"),
			"S": random.choice("CG"),
			"Y": random.choice("CT"), "K": random.choice("GT"), "V": random.choice("ACG"),
			"H": random.choice("ACT"),
			"D": random.choice("AGT"), "B": random.choice("CGT"), "X": random.choice("GATC"), "N": random.choice("GATC")
		}
		# maakt van vreemde letters
		if a in nuc_key:
			a = nuc_key[a]

		if a == "A" or a == "a":
			direc['A{}'.format(pos)] = direc.setdefault('A{}'.format(pos), 0)+1

		elif a == "C" or a == "c":
			direc['C{}'.format(pos)] = direc.setdefault('C{}'.format(pos), 0)+1

		elif a == "T" or a == "t":
			direc['T{}'.format(pos)] = direc.setdefault('T{}'.format(pos), 0)+1

		elif a == "G" or a == "g":
			direc['G{}'.format(pos)] = direc.setdefault('G{}'.format(pos), 0)+1

		else:
			direc['{}{}'.format(a, pos)] = direc.setdefault('{}{}'.format(a, pos), 0)+1
			#if a == "N":
				#print(a)

consensus = ">EIGEN_CONSENSUS\n"

for seq in sequences:
	g = 0
	h = 0
	for a in seq:
		g += 1
		pos = g-1
		res = {key: direc[key] for key in direc.keys() & {'A{}'.format(pos), 'G{}'.format(pos), 'T{}'.format(pos), 'C{}'.format(pos), '{}{}'.format(a, pos)}}
		#if "T5842" in res:
			# print(res)
		# kijk of de volgorde in de res random zijn zo ja dan moeten er nog steeds dingen gebeuren maar dan hoef ik me geen zorgen te maken over de values die een verschil van 2 hebben.
		letter = max(res.keys(), key=(lambda k: res[k]))
		print(letter + ",")
		if letter[0] == "-":
			del res['-{}'.format(pos)]
			#print(res)
			try:
				letter = max(res.keys(), key=(lambda k: res[k]))
				consensus += letter[0]
			except ValueError:
				consensus += ""
		else:
			letter = letter[0]
			consensus += letter
		h = 2
	if h == 2:
		break

f = open("/students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/eigen_consensus.fasta", "w+")
f.write(consensus)
f.close()


def main():
	print(consensus)


if __name__ == "__main__":
	main()
=======
#!/usr/bin/env python3
import argparse
import re
import sys
import collections
import Bio
import operator
"""
from Bio.Seq import Seq
from Bio.Align.Generic import Alignment
from Bio.Align import AlignInfo
from Bio.Alphabet import IUPAC, Gapped
  
from Bio._py3k import raise_from 
from Bio.Seq import Seq 
from Bio.SeqRecord import SeqRecord, _RestrictedDict 
from Bio import Alphabet 
from Bio.Align import _aligners 

align = AlignIO.read("/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/region_output.fa", "fasta") 
summary_align = AlignInfo.SummaryInfo(align)
consensus = summary_align.dumb_consensus()
"""

sequence = ''
op_file = open("/students/2019-2020/Thema11/minion_js_jh/msa/msa_output/region_output.fa")
for line in op_file:
	if line.startswith(">"):
		line = line.replace(line,"#")
		sequence += line
	else:
		sequence += line
sequence = sequence.replace("\n", "")
sequences = sequence.split("#")

direc = {}

for seq in sequences:
	g=0
	for a in seq:
		g += 1
		#dictionary met elke posite de actgoverige als keys en aantal als value.
		pos = g-1
		
		if a == "A" or a == "a":
			direc['A{}'.format(pos)]=direc.setdefault('A{}'.format(pos), 0)+1
			
		elif a == "C" or a == "c":
			direc['C{}'.format(pos)]=direc.setdefault('C{}'.format(pos), 0)+1
		
		elif a == "T" or a == "t":
			direc['T{}'.format(pos)]=direc.setdefault('T{}'.format(pos), 0)+1
		
		elif a == "G" or a == "g":
			direc['G{}'.format(pos)]=direc.setdefault('G{}'.format(pos), 0)+1
			
		else:
			direc['{}{}'.format(a,pos)]=direc.setdefault('{}{}'.format(a,pos), 0)+1

consensus = ">EIGEN_CONSENSUS\n"	
		
for seq in sequences:
	g=0
	h=0
	for a in seq:
		g += 1
		pos = g-1
		res = {key: direc[key] for key in direc.keys() & {'A{}'.format(pos), 'G{}'.format(pos), 'T{}'.format(pos), 'C{}'.format(pos), '{}{}'.format(a,pos)}}
		letter = max(res.keys(), key=(lambda k: res[k]))
		if letter[0] == "-":
			del res['-{}'.format(pos)]
			try:
				letter = max(res.keys(), key=(lambda k: res[k]))
			except ValueError:
				consensus += "-"
		else:
			letter = letter[0]
			consensus += letter
		h = 2
	if h==2:
		break
	


f= open("/students/2019-2020/Thema11/minion_js_jh/msa/consensus_sequence/eigen_consensus.fasta","w+")
f.write(consensus)
f.close() 
def main():
     #print(sequences)
     #print(direc)
     print(consensus)
     
if __name__ == "__main__":
     main()
>>>>>>> 36b572ec7e8d217b055fcf0a44ff26241f012dea
